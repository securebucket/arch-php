<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/
 */

/**
 * Load the core file
 */
require 'plug-ins/_required/_arch.php';

/**
 * Load required core classes
 */
arch::ClassScan(DIR_CORE_REQUIRED, 'core');

// wrap it in a try statement

    /** 
     * starts the error handler and header setter
     */
    arch::cls('integrity');

    /**
     * Make sure php, and apache are in a safe enviorment
     */
    arch::cls('SystemCheck');

    /**
     * Create a virtual enforcer that makes you follow good standards 
     * when dealing with outside data.
     */
    arch::cls('enforcer');

    /**
     * Core Optional Packaged Plug-ins Loader
     */
    arch::OptionalCorePlugins();

    /**
     * Include Custom Plug-Ins
     */
    arch::ClassScan(DIR_PLUGINS, 'plugins');

    /**
     * Include Model Files
     */
    arch::ClassScan(DIR_MODELS, 'models');

?>