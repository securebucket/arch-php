<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006

 */

class integrity {

    private $level, $log, $page, $line;

    /**
     *  sets error reporting to strict
     * also, sets the error handler
     */
    public function __construct() {

        if(!defined('SERVER_DOMAIN')) { echo 'missing config constant SERVER_DOMAIN in config.php file.'; exit; }
        
        ob_start(array($this, 'archError'));

        error_reporting("E_ALL | E_STRICT");

        set_error_handler(array($this, 'error_handler'));

        $this->errorcompiled = '';
    }

    /**
     * will forward request to the 404 page.  
     * The uback var will supply the page 
     * the request came from.   
     */
    public function error_404() {
        header("HTTP/1.1 404 Not Found");
        header("Status: 404 Not Found");
        header("Location: " . ERROR_PAGES_404 . "?uback=" . base64_encode(evar::server('REQUEST_URI', 'url')));
        header("Connection: close");
        exit();
    }

    /*
     * will supply the browser with a 403 denied header.
     */

    public function error_403() {
        header("HTTP/1.1 403 Forbidden");
        header("Location: " . ERROR_PAGES_403);
        header("Connection: close");
        exit();
    }

    /*     * integrity:
     * forwards to a new page and sends the browser the correct headers.
     * this will also forward your google tracking code too.
     * @param < url to forward to > $url 
     */

    public function error_301($url) {
        // will transfer the google tracking code =)
        $temp = evar::get('gclid');
        if (!empty($temp)) {
            if (stristr($url, '?')) {
                $url = $url . '&' . $temp;
            } else
                $url = $url . '?' . $temp;
        }
        unset($temp);
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: $url");
        header("Connection: close");
        exit();
    }

    public function error_302($url) {
        // will transfer the google tracking code =)
        $temp = evar::get('gclid');
        if (!empty($temp)) {
            if (stristr($url, '?')) {
                $url = $url . '&' . $temp;
            } else
                $url = $url . '?' . $temp;
        }
        unset($temp);
        header("HTTP/1.1 302 Moved Temporarily");
        header("Location: $url");
        header("Connection: close");
        exit();
    }

    /**
     * this will supply a 500 error and either echo if ADMIN_EMAIL isn't set
     * or it will send the error to your email.
     * @param string $data 
     */
    public function error_500($data) {

        /*
         * set the subject of the error
         */
        $subject = SERVER_DOMAIN . ": Arch PHP caught an Error";

        /*
         * if ADMIN_EMAIL isn't set in the config file.  
         * Then print the error.
         */
        $this->errorcompiled[] = $subject . '  ' . $data;
    }

    public function error_handler($errno, $errstr, $errfile, $errline) {
        self::error_500("$errno,$errstr,$errfile,$errline");
    }

    /**
     * attempts to catch fatal errors
     * @param output-buffer $buffer
     * @return string 
     */
    public function archError($buffer) {
        /**
         * checks for the term fatal error.  
         * Limits the stristr for speed reasons 
         */
        $error = error_get_last();
        $output = "";
        foreach ($error as $info => $string)
            $output .= "{$info}: {$string}\n";
        if (!empty($output))
            $error = self::error_500($output);
        else
            $error = '';
        return $error . $buffer;
    }

    function __destruct() {
        $e = ADMIN_EMAIL;
        if (empty($e) && !empty($this->errorcompiled)) {
            print_r($this->errorcompiled);
        } elseif(!empty($this->errorcompiled)) {
            $mail_body = SERVER_DOMAIN . ": Arch PHP caught an Error a 
                                    user Encountered...\n\n" . print_r($this->errorcompiled,true);
            $subject = SERVER_DOMAIN . ": Arch PHP caught an Error";
            $header = "From: " . ADMIN_NAME . " <" . ADMIN_EMAIL . ">\r\n";

            mail(ADMIN_EMAIL, $subject, wordwrap($mail_body, 70), $header);
            
        }
    }

}

?>