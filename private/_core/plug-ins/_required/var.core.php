<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class evar {

    /**
     * Will retrieve and clean the requested $_POST
     * @param <string> $field   field of the request post
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    static function post($field, $format = 'char') {
        return self::CleanData(self::getVar('POST', $field), $format);
    }

    /**
     * Will retrieve and clean the requested $_GET
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    static function get($field, $format = 'char') {
        return self::CleanData(self::getVar('GET', $field), $format);
    }

    /**
     * Will retrieve and clean the requested $_REQUEST
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string> 
     */
    static function request($field, $format = 'char') {
        return self::CleanData(self::getVar('REQUEST', $field), $format);
    }

    /**
     * Will retrieve and clean the requested $_SERVER
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    static function server($field, $format = 'char') {
        return self::CleanData(self::getVar('SERVER', $field), $format);
    }

    /**
     * Will retrieve and clean the requested $_COOKIE
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    static function cookie($field, $format = 'char') {
        return self::CleanData(self::getVar('COOKIE', $field), $format);
    }

    /**
     * Will retrieve and clean the requested $_SESSION
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    static function session($field, $format = 'char') {
        return self::CleanData(self::getVar('SESSION', $field), $format);
    }

    /**
     * Processes the request for the above methods.
     * @param <string> $field   field of the request get
     * @param <string> $format  format field should be in.
     * @return <string>
     */
    private static function getVar($type, $field) {
        if (!isset(arch::cls("enforcer")->blocked["_" .
                        self::CleanData($type, 'field') . ""]
                        [self::CleanData($field, 'field')]))
            return false;

        $variable = eval('return arch::cls("enforcer")->blocked["_' .
                self::CleanData($type, 'field') . '"]["' .
                self::CleanData($field, 'field') . '"]' . ";");

        if (isset($variable) && !empty($variable))
            return $variable;
        else
            return false;
    }

    /**
     * Will remove double spaces and end with a trim.
     * If line breaks = false is set, it will remove \n\t\r.
     * @param <string> $text            $text to be modified
     * @param <boolean> $lineBreaks     line breaks outputted?
     * @return <type>
     */
    static function RemoveExtraSpaces($text, $lineBreaks = false) {
        if (!$lineBreaks) {
            return preg_replace("/[[:blank:]]+/", " "
                            , trim(str_replace(array("\n", "\r\n", "\r", "\t"), '', $text)));
        } elseif ($lineBreaks) {
            return preg_replace("/[[:blank:]]+/", " ", trim(str_replace("\n", '', $text)));
        }
    }

    /**
     * Output will only be AlphaNumeric Characters
     * @param <string> $text            text to be modified
     * @param <boolean> $spacesAllowed  Do you want spaces?  Boolean
     * @return <string>
     */
    static function AlphaNumericOnly($text, $spacesAllowed = false) {
        if ($spacesAllowed) {
            return self::RemoveExtraSpaces(preg_replace("{[^a-zA-z0-9 ]}", '', $text));
        } elseif (!$spacesAllowed) {
            return preg_replace("{[^a-zA-z0-9]}", '', $text);
        }
    }

    /**
     * Var Cleaner function.
     * @param <string> $data    text to be scanned
     * @param <enum> $type      output? (int|ip|text|char|field|url)
     * @param <boolean> $spaces output spaces?
     * @return <type>
     */
    static public function CleanData($data, $type = 'char', $spaces = true) {
        if ($data === '') {
            return '';
        }

        if ($type === 'int') {
            $data = (int) preg_replace("{[^0-9]}", '', $data);
        } elseif ($type === 'ip') {
            $data = (string) preg_replace("{[^0-9.]}", '', $data);
        } elseif ($type === 'text') {
            $data = self::RemoveExtraSpaces($data, true);
            $data = strip_tags($data);
        } elseif ($type == 'alphanum') {
            $data = self::AlphaNumericOnly($data, true);
        } elseif ($type == 'char') {
            $data = self::RemoveExtraSpaces($data);
            $data = self::AlphaNumericOnly($data, true);
        } elseif ($type == 'field') {
            $data = preg_replace("{[^a-zA-z0-9-_]}", '', $data);
        } elseif ($type == 'url') {
            $data = preg_replace('{[^a-zA-z0-9:+/.&?%=-]}', '', $data);
        } elseif($type == 'default-reverse') {
            $data = self::AlphaNumericOnly($data, true);
            $data = self::RemoveExtraSpaces($data);
        } else {
            $data = self::RemoveExtraSpaces($data);
            $data = self::AlphaNumericOnly($data, true);
        }


        if (!$spaces) {
            $data = str_replace(' ', '', $data);
        }

        return $data;
    }

    /**
     * String Escape function that makes
     * sure strings aren't double escaped.
     * @param <string> $str     Text to addslashes too
     * @return <string>
     */
    static function StringEscape($str) {
        $len = strlen($str);
        $escapeCount = 0;
        $targetString = '';
        for ($offset = 0; $offset < $len; $offset++) {
            switch ($c = $str{$offset}) {
                case "'":
                    // Escapes this quote only if its not preceded by an unescaped backslash 
                    if ($escapeCount % 2 == 0)
                        $targetString.="\\";
                    $escapeCount = 0;
                    $targetString.=$c;
                    break;
                case '"':
                    // Escapes this quote only if its not preceded by an unescaped backslash 
                    if ($escapeCount % 2 == 0)
                        $targetString.="\\";
                    $escapeCount = 0;
                    $targetString.=$c;
                    break;
                case '\\':
                    $escapeCount++;
                    $targetString.=$c;
                    break;
                default:
                    $escapeCount = 0;
                    $targetString.=$c;
            }
        }
        return $targetString;
    }

    /**
     * For the rare case you need to split a
     * name into two fields.  This is used for CC companies.
     * @param <string> $name  name to be split
     * @return <array> (compilefname | compilelname )
     */
    static function ParseName($name) {
        $parseB = explode(" ", $name);
        $titleT = '';
        foreach ($parseB as $key => $value) {
            $value = preg_replace('/[^0-9A-Za-z]/', '', $value);
            if ($value != "") {
                $parse[] = $value;
            }
        }
        $count = count($parse) - 1;

        $title = array('Mr', 'Mrs', 'Ms', 'Dr', 'Mme', 'Mssr', 'Mister', 'Miss'
            , 'Doctor', 'Sir', 'Lord', 'Lady', 'Madam', 'Mayor', 'President');
        $pedigree = array('Jr', 'Sr', 'III', 'IV', 'VIII', 'IX', 'XIII');

        for ($i = 0; $i < count($parse); $i++) {
            if ($parse[$i] != "") {
                if ($i == 0) {
                    foreach ($title as $key => $value) {
                        if ($parse[$i] == $value) {
                            $build['title'] = $parse[$i];
                            $titleT = 't';
                        }
                    }
                    if ($titleT != 't') {
                        $build['fname'] = $parse[$i];
                    }
                }
                if (($i > 0) && ($count != $i)) {
                    $testI = $i + 1;
                    if ($titleT == 't') {
                        $build['fname'] = $parse[$i];
                        unset($titleT);
                    } elseif ($titleT != 'f') {
                        $titleT = 'f';
                        $build['mname'] = $parse[$i];
                    } else {
                        foreach ($pedigree as $key => $value) {
                            if ($parse[$testI] == $value) {
                                $build['lname'] = $parse[$i];
                                $notFound = 'T';
                            }
                        }
                        if ($notFound != 'T') {
                            $build['mname'] .= " " . $parse[$i];
                        }
                    }
                }
                if ($count == $i) {
                    foreach ($pedigree as $key => $value) {
                        if ($parse[$i] == $value) {
                            $build['pedigree'] = $parse[$i];
                        }
                    }

                    if (!isset($build['pedigree']) || $build['pedigree'] == "") {
                        $build['lname'] = $parse[$i];
                    }
                }
            }
        } // end for
        if (!isset($build['compilefname']))
            $build['compilefname'] = '';
        if (!isset($build['compilelname']))
            $build['compilelname'] = '';
        if ($build['lname'] == "") {
            $build['lname'] = $build['mname'];
            unset($build['mname']);
        }
        if (isset($build['title']) && $build['title'] != "") {
            $build['compilefname'] = "$build[title] ";
        }
        if (isset($build['fname']) && $build['fname'] != "") {
            $build['compilefname'] .= "$build[fname] ";
        }
        if (isset($build['mname']) && $build['mname'] != "") {
            $build['compilefname'] .= "$build[mname] ";
        }
        if (isset($build['lname']) && $build['lname'] != "") {
            $build['compilelname'] = "$build[lname] ";
        }
        if (isset($build['pedigree']) && $build['pedigree'] != "") {
            $build['compilelname'] .= "$build[pedigree] ";
        }

        return $build;
    }

}

?>