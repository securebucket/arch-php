<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class arch {

    private $loaders, $class;

    /**
     *  class handler.  loads into a instance
     *  and limits wasteful loading.
     *  http://archphp.org/docs#4eb6e35011aec61146000006
     * @staticvar array $loaders
     * @param string $class
     * @param <type> $renameInstance
     * @return class
     */
    static function cls($class, $renameInstance = 'default') {
        static $loaders = array();

        if ($renameInstance === 'default')
            $renameInstance = $class;

        if (
                class_exists($class)
                && !array_key_exists($renameInstance, $loaders)
        ) {
            $loaders[$renameInstance] = new $class();
        }
        return $loaders[$renameInstance];
    }

    /**
     * Scans given folder for matches (from search term)
     * Then responds with a array of file names.
     * @staticvar array $includedArray
     * @param string $directory
     * @param string $search
     */
    static function ClassScan($directory, $search) {
        try {
            static $includedArray = array();

            $path = realpath($directory);
            $objects = new RecursiveIteratorIterator(
                            new RecursiveDirectoryIterator($path),
                            RecursiveIteratorIterator::SELF_FIRST);

            foreach ($objects as $name => $object) {
                if (
                        stristr($name, $search . '.php')
                        && !empty($name)
                        && !stristr($name, '..')
                        && !stristr($name, '/.')
                        && !stristr($name, 'php~')
                        && !in_array($name, $includedArray)
                ) {

                    require $name;
                    $includedArray[] = $name;
                }
            }
        } catch (Exception $e) {
            trigger_error('Caught exception - Loading Class File: ' .
                    $e->getMessage() . "<br>");
        }
    }

    /**
     * This is the loader for the optional core components available.
     * Controlled from the config.inc.php file.
     * http://archphp.org/docs#4eb6df6811aec62f44000007
     */
    static function OptionalCorePlugins() {
        try {
            if (CORE_OPT_PLUGIN_MYSQL === '1')
                if (CORE_OPT_PLUGIN_MYSQL_TYPE === 'pdo') {
                    require DIR_CORE_OPTIONAL . 'db.mysql.pdo.plugins.php';
                } else {
                    require DIR_CORE_OPTIONAL . 'db.mysql.mysqli.plugins.php';
                }
            if (CORE_OPT_PLUGIN_MONGODB === '1')
                require DIR_CORE_OPTIONAL . 'db.mongodb.plugins.php';
            if (CORE_OPT_PLUGIN_FILES === '1')
                require DIR_CORE_OPTIONAL . 'file.plugins.php';
            if (CORE_OPT_PLUGIN_IMAGE === '1')
                require DIR_CORE_OPTIONAL . 'image.plugins.php';
        } catch (Exception $e) {
            trigger_error('Caught exception - Loading Optional Class File: ' .
                    $e->getMessage() . "<br>");
        }
    }

    /**
     * Will send a email to the admin setup in the config.inc.php file.
     * @param string $message
     * http://archphp.org/docs#4eb6df6811aec62f44000007
     */
    static public function sendLog($message) {

        $mail_body = "Don't reply to this email.\nThis is a notification...\n\n"
                . $message . "\n\n"; //.$urlmail body
        $header = "From: noreply <noreply@root>\r\n"; //optional headerfields

        mail(ADMIN_EMAIL, 'Message you should see', $mail_body, $header); //mail command :)
    }

}

?>