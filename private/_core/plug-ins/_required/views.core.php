<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class views {

    /**
     * Stores sections for later includes.  
     * Like Javascript Jquery code that should be added above </body> tag
     * @staticvar array $sectionIncludes holds the information
     * @param string $name name of section ['javascript']['css']
     * @param string $data the information that should be saved
     * @param boolean $return should this return the array.
     * @param type $priority priority allows you to sort it will sort from 0 to 10
     */
    static public function section($return = false, $name = '', $data = '', $priority = '0') {
        static $sectionIncludes = array();

        if ($return === true) {
            return $sectionIncludes;
        }

        if ($name === 'url')
            $sectionIncludes['url'] = $data;
        elseif ($name === 'navSelect')
            $sectionIncludes['navSelect'] = $data;
        else {
            if (!isset($sectionIncludes[$name]))
                $sectionIncludes[$name] = array();

            if (!isset($sectionIncludes[$name][$priority]))
                $sectionIncludes[$name][$priority] = array();

            $sectionIncludes[$name][$priority][] = $data;
        }
    }

    /**
     *  Loads a view file.  You can supply $data with a array or single.
     * If you have a css/js file in that view folder.  It will also be
     * loaded.
     * @param <string> $path        file to render e.g '_core/template.php'
     * @param <array|string> $data  content used in the file called.
     * @param <boolean> $returnVar  if true. Content will be returned in a array
     * @return <array>              ['content'] has the data outputted
     */
    static public function load($path, $data = array(), $returnVar = false, $compress = false) {
        $return = array();
        // make sure it's not the core view folder.  
        // This is loaded when going to /media/js and /media/css
        if (!stristr($path, '_core')) {
            if (!defined('INCLUDE_EMBED_JS'))
                define('INCLUDE_EMBED_JS'
                        , self::MediaScanInclude(DIR_VIEWS . $path, 'js'));
            if (!defined('INCLUDE_EMBED_CSS'))
                define('INCLUDE_EMBED_CSS'
                        , self::MediaScanInclude(DIR_VIEWS . $path, 'css'));
        }

        if (substr($path, -4) === '.css')
            $path .= '';
        elseif (substr($path, -3) === '.js')
            $path .= '';
        elseif (substr($path, -5) === '.less')
            $path .= '';
        elseif (substr($path, -5) === '.html')
            $path .= '';
        elseif (substr($path, -4) != '.php')
            $path .= '.php';

        ob_start();
        include(DIR_VIEWS . $path);
        $return['content'] = ob_get_contents();
        ob_end_clean();

        if (!$returnVar) {
            if($compress) echo evar::RemoveExtraSpaces ($return['content']);
            else echo $return['content'];
        } else {
            return $return;
        }
    }

    /**
     * Will scan a folder for css/js files, and combine them.
     * @param <string> $directory   directory that should be scanned
     * @param <string> $search      only return files that match filename
     * @return <string>               return the combined media files
     */
    static function MediaScanInclude($directory, $search) {
        try {
            $build = '';

            $path = dirname($directory);
            $path .= '/' . $search;

            $objects = new RecursiveIteratorIterator(
                            new RecursiveDirectoryIterator($path),
                            RecursiveIteratorIterator::SELF_FIRST);

            foreach ($objects as $name => $object) {

                if (
                        stristr($name, '.' . $search)
                        && !empty($name)
                        && !stristr($name, $search . '~')
                        && !stristr($name, '/.')
                ) {

                    ob_start();

                    include $name;

                    $build .= ob_get_contents();
                    ob_end_clean();
                }
            }
        } catch (Exception $e) {
            trigger_error('Caught exception - Loading Class File: '
                    . $e->getMessage() . "<br>");
        }
        return $build;
    }

    /**
     * Loads specific files, specified in the config.inc.php file.
     * @param <enum> $type      css | js
     * @param <string> $data    csv of files.  Will be loaded in order
     * @return <type>           returns outputted combined content
     */
    static function MediaInclude($type, $data) {
        try {
            $build = '';
            ob_start();

            if ($type === 'css') {
                foreach (explode(',', $data) as $cssinclude) {
                    if (!empty($cssinclude))
                        include DIR_VIEWS_CORE_CSS . $cssinclude;
                }
            } elseif ($type === 'js') {
                foreach (explode(',', $data) as $jsinclude) {
                    if (!empty($jsinclude))
                        include DIR_VIEWS_CORE_JS . $jsinclude;
                }
            }

            $build .= ob_get_contents();
            ob_end_clean();

            return $build;
        } catch (Exception $e) {
            trigger_error('Caught exception - Loading Optional Class File: '
                    . $e->getMessage() . "<br>");
        }
    }

    public static function render($data = array(), $compress = false) {

        $temp = self::section(true);

        if (isset($temp['js'])) {
            krsort($temp['js']);
            $data['js'] = array();
            foreach ($temp['js'] as $jsarray) {
                $temp['jst'][] = implode(' ', $jsarray);
            }
            $data['js'] = implode('', $temp['jst']);
        }

        if (isset($temp['css'])) {
            krsort($temp['css']);
            $data['css'] = array();
            foreach ($temp['css'] as $cssarray) {
                $temp['csst'][] = implode(' ', $cssarray);
            }
            $data['css'] = implode('', $temp['csst']);
        }

        if (isset($temp['url']))
            $data['url'] = $temp['url'];

        if (isset($temp['navSelect']))
            $data['navSelect'] = $temp['navSelect'];

        views::load('_core/template', $data, false, $compress);
        exit;
    }

}

?>