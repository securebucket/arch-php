<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6f4ba11aec62044000006
 */

class cache {

    /**
     * stores the class name that will be called.
     * @param string $class
     */
    public function setclass($class) {
        $this->className = $class;
    }

    /**
     * If using File Cache enter the directory.  
     * If using a database.  Enter the table name.
     * @param string $set
     */
    public function setPointer($set) {
        $this->organizer = $set;
    }

    /**
     * Used to get cache by key from the selected cache controller.
     * @param string $key
     * @return array( data => ''... )
     */
    public function get($key) {
        return arch::cls($this->className)->getCache($key);
    }

    /**
     * Will put something in cache.
     * @param string $key
     * @param string $value
     * @return boolean
     */
    public function put($key, $value) {
        return arch::cls($this->className)->putCache($key, $value);
    }

    /**
     * Will remove an item from the cache.
     * @param string $key
     * @return boolean
     */
    public function del($key) {
        return arch::cls($this->className)->delCache($key);
    }

}

?>