<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class enforcer {

    function __construct() {
        $this->TransferUnsafe();
        $this->RemoveUnsafe();
    }

    /**
     * Will transfer all the enviormental variables
     * to a different variable.
     */
    function TransferUnsafe() {
        if (isset($_GET))
            $this->blocked['_GET'] = $_GET;
        if (isset($_POST))
            $this->blocked['_POST'] = $_POST;
        if (isset($_REQUEST))
            $this->blocked['_REQUEST'] = $_REQUEST;
        if (isset($_ENV))
            $this->blocked['_ENV'] = $_ENV;
        if (isset($_SERVER))
            $this->blocked['_SERVER'] = $_SERVER;
        if (isset($_SESSION))
            $this->blocked['_SESSION'] = $_SESSION;
        if (isset($_COOKIE))
            $this->blocked['_COOKIE'] = $_COOKIE;
        if (isset($_FILES))
            $this->blocked['_FILES'] = $_FILES;
    }

    /**
     * Once the variables have been transfered.  Wipe them.
     */
    function RemoveUnsafe() {
        foreach ($GLOBALS as $key => $blank) {
            if ($key != 'GLOBALS') {
                unset($GLOBALS[$key]);
            }
        }
        unset($_GET, $_POST, $_REQUEST, $_ENV, $_SERVER, $_SESSION,$_FILES);
    }

}
?>