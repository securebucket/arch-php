<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class SystemCheck {

    function __construct() {
        /*
         * Check for Required Modules are loaded
         */
        self::depends(
                        array(
                            array('apc', 'php-ext'),
                            array('mail', 'function'),
                            array('zlib.output_compression', 'ini', '1'),
                            array('allow_url_fopen', '0'),
                            array('display_errors', '0'),
                            array('display_startup_errors', '0'),
                            array('log_errors', '1'),
                            array('error_reporting', 'E_ALL'),
                            array('expose_php', '0'),
                            array('magic_quotes_gpc', '0'),
                            array('magic_quotes_sybase', '0'),
                            array('register_globals', '0')
                        )
        );
    }

    /**
     * Dependency Check. 
     * @param array $array( [ name of function | class | ini ) ] =>  [ type  of function | class | ini ] ) 
     */
    public function depends($array = array()) {
        $goodtogo = true;
        foreach ($array as $list) {

            // $name = the name of the function/class/ini
            // $type = the type of name being checked.
            // $value is in the case you need to check the value (php.ini only)
            // array(1 => null, null) is to keep undefined error on unused vars
            list($name, $type, $value) = $list + array(1 => null, null);

            $failed = false;

            if (empty($name) OR empty($type))
                continue;

            switch ($type) {
                case 'class':
                    if (!class_exists($name))
                        $failed = true;
                    break;
                case 'function':
                    if (!function_exists($name))
                        $failed = true;
                    break;
                case 'php-ext':
                    if (!extension_loaded($name))
                        $failed = true;
                    break;
                case 'ini':
                    if ((int) ini_get($name) != $value)
                        $failed = true;
                    break;
            }

            if ($failed) {
                trigger_error($name . '(' . $type . ') is disabled ' . $value . '<br>');
                $goodtogo = false;
            }
            //else
            //    echo $name . '(' . $type . ') is active' . $value . '<br>';
        }

        return $goodtogo;
    }

}
?>