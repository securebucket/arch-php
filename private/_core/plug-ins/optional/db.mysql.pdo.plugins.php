<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

/**
 * SystemCheck will make sure system has mysql pdo support.
 * If not, then it won't load.
 */
if (
        !arch::cls('SystemCheck')->depends(
                array(
                    array('PDO', 'class'),
                    array('PDO', 'php-ext'),
                    array('mysql', 'php-ext')
                )
        )
)
    goto SkipClassLoad;

class sql extends PDO {

    /**
     * Settings are set in the config.inc.php file.
     */
    public function __construct() {
        parent::__construct(DB_MYSQL_TYPE . ':host=' . DB_MYSQL_HOST
                . ';dbname=' . DB_MYSQL_DBNAME, DB_MYSQL_DBUSER
                , DB_MYSQL_DBPASS);

        /*
         * Set PDO to send errors to the error handler.
         */
        self::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function get($settings = array()) {
        if (count($settings) === 0)
            return;

        if (isset($settings['query']))
            $query = " WHERE {$settings['query']} ";
        else
            $query = '';

        if (isset($settings['order']))
            $query .= " ORDER BY `{$settings['order']}` ";

        if (isset($settings['limit']))
            $query .= " LIMIT {$settings['limit']} ";

        $sth = self::prepare('SELECT ' . $settings['fieldList'] . '
                FROM `' . $settings['table'] . '` ' . $query);
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        return $sth;
    }

    public function up($settings = array()) {

        $query = '';

        if (!is_array($settings['fields']))
            return;

        foreach ($settings['fields'] as $field => $value) {
            $prepare[] = '`' . $field . '` = :' . str_replace('-', '_', $field);
            $execute[':' . str_replace('-', '_', $field)] = $value;
        }
        unset($settings['fields']);
        if (isset($settings['query']))
            $query = " WHERE {$settings['query']} ";
        else
            $query = '';

        if (isset($settings['order']))
            $query .= " ORDER BY `{$settings['order']}` ";

        if (isset($settings['limit']))
            $query .= " LIMIT {$settings['limit']} ";

        $sth = self::prepare('UPDATE `' . $settings['table'] . '`
                SET ' . implode(',', $prepare) . $query);

        return $sth->execute($execute);
    }

    public function in($settings = array()) {

        $query = '';

        if (!is_array($settings['fields']))
            return;

        foreach ($settings['fields'] as $field => $value) {
            $prepareFields[] = '`' . $field . '`';
            $prepareParam[] = ' :' . str_replace('-', '_', $field);
            $execute[':' . str_replace('-', '_', $field)] = $value;
        } unset($settings['fields']);

        $sth = self::prepare('INSERT INTO `' . $settings['table'] . '`
                (' . implode(',', $prepareFields) . ') VALUES (' .
                        implode(',', $prepareParam) . ')');
        return $sth->execute($execute);
    }

}

/**
 * Load SQL DB Plugin
 * If not, then it won't load.
 */
arch::cls('sql');

SkipClassLoad:
?>