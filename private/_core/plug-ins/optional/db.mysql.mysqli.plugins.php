<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

/**
 * SystemCheck will make sure system has mysql pdo support.
 * If not, then it won't load.
 */
if (
        !arch::cls('SystemCheck')->depends(
                array(
                    array('mysqli', 'class'),
                    array('mysqli', 'php-ext'),
                    array('mysql', 'php-ext')
                )
        )
)
    goto SkipClassLoad;

class sql extends mysqli {

    /**
     * Settings are set in the config.inc.php file.
     */
    public function __construct() {
        parent::__construct(DB_MYSQL_HOST, DB_MYSQL_DBUSER, DB_MYSQL_DBPASS, DB_MYSQL_DBNAME);
        $this->set_charset("utf8");
        self::debug();

        /*
         * Set PDO to send errors to the error handler.
         */
        //self::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function get($settings = array()) {
        if (count($settings) === 0)
            return;

        if (isset($settings['query']))
            $query = " WHERE {$settings['query']} ";
        else
            $query = '';

        if (isset($settings['order']))
            $query .= " ORDER BY {$settings['order']} ";

        if (isset($settings['limit']))
            $query .= " LIMIT {$settings['limit']} ";

        if (isset($settings['table']) && stristr($settings['table'], 'as'))
            $settings['from'] = "FROM  {$settings['table']}";
        elseif (isset($settings['table']) && !empty($settings['table']))
            $settings['from'] = "FROM `{$settings['table']}`";
        else
            $settings['from'] = '';

        $sql = 'SELECT ' . $settings['fieldList'] . '
                ' . $settings['from'] . ' ' . $query;

        if ($this->debug)
            echo $sql . "\n";

        $this->last_sql = $sql;

        $res = self::query($sql)
                or trigger_error("Prepared Select Statement Error: " . $this->error . "\n");

        return $res;
    }

    public function up($settings = array()) {

        $query = '';
        $prepare = array();
        $execute = array();
        $params = array();

        if (!is_array($settings['fields']))
            return;

        foreach ($settings['fields'] as $field => $value) {
            $prepare[] = '`' . $field . '` = ?';
            $execute[':' . str_replace('-', '_', $field)] = $value;
            $params[':' . str_replace('-', '_', $field)] = 's';
        }
        unset($settings['fields']);
        if (isset($settings['query']))
            $query = " WHERE {$settings['query']} ";
        else
            $query = '';

        if (isset($settings['order']))
            $query .= " ORDER BY `{$settings['order']}` ";

        if (isset($settings['limit']))
            $query .= " LIMIT {$settings['limit']} ";

        $sql = 'UPDATE `' . $settings['table'] . '`
                SET ' . implode(',', $prepare) . $query;

        if ($this->debug)
            echo $sql . "\n";

        $this->last_sql = $sql;

        $sth = self::prepare($sql);

        $args = array();
        $args[] = implode('', array_values($params));

        foreach ($params as $paramName => $paramType) {
            $args[] = &$params[$paramName];
            $params[$paramName] = $execute[$paramName];
        }

        call_user_func_array(array(&$sth, 'bind_param'), $args);


        if ($release = $sth->execute()) {
            
        } else {
            trigger_error("Prepared Statement Error: " . $this->error . "\n");
        }

        return $sth;
    }

    public function in($settings = array()) {

        $query = '';
        $params = array();
        $execute = array();
        $params = array();
        $prepareFields = array();

        if (!is_array($settings['fields']))
            return;

        foreach ($settings['fields'] as $field => $value) {
            $prepareFields[] = '`' . $field . '`';

            if ($value === 'NOW()') {
                $prepareParam[] = ' NOW()';
            } else {
                $prepareParam[] = ' ?';
                $execute[':' . str_replace('-', '_', $field)] = $value;
                $params[':' . str_replace('-', '_', $field)] = 's';
            }
        } unset($settings['fields']);

        if (isset($settings['replace']))
            $querytype = 'REPLACE';
        else
            $querytype = 'INSERT';

        $sql = $querytype . ' INTO `' . $settings['table'] . '`
                (' . implode(',', $prepareFields) . ') VALUES (' .
                implode(',', $prepareParam) . ')';

        if ($this->debug)
            echo $sql . "\n";

        $this->last_sql = $sql;

        $sth = self::prepare($sql);

        $args = array();
        $args[] = implode('', array_values($params));

        foreach ($params as $paramName => $paramType) {
            $args[] = &$params[$paramName];
            $params[$paramName] = $execute[$paramName];
        }

        call_user_func_array(array(&$sth, 'bind_param'), $args);

        if ($release = $sth->execute()) {
            
        } else {
            trigger_error("Prepared Statement Error: " . $this->error . "\n");
        }

        return $sth;
    }

    /**
     * If debug switch is set to true.  It will echo all queries
     * passed to the controller.
     * @param boolean $mode true | false
     */
    function debug($mode = false) {
        $this->debug = (bool) $mode;
    }

    function __destruct() {
        self::close();
    }

}

/**
 * Load SQL DB Plugin
 * If not, then it won't load.
 */
arch::cls('sql');

SkipClassLoad:
?>