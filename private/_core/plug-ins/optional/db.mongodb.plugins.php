<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */


/**
 * SystemCheck will make sure system has mongo support.
 * If not, then it won't load.
 */
if (
        !arch::cls('SystemCheck')->depends(
                array(
                    array('Mongo', 'class'),
                    array('mongo', 'php-ext')
                )
        )
)
    goto SkipClassLoad;

class mdb extends Mongo {

    /**
     * Creates connection to mongodb
     */
    public function __construct() {
        parent::__construct("mongodb://" . DB_MONGODB_DBUSER . ":"
                        . DB_MONGODB_DBPASS . "@" . DB_MONGODB_HOST . "");
    }

    /**
     * These override the default caching system.
     * @param <string> $key     $cache uniqueid
     * @param <string> $data    $data to be cached
     * @return <boolean>        successul or not.
     */
    function putCache($key, $data) {
        return self::selectDB("cache")->
                {arch::cls('cache')->organizer}->
                update(array(
                    '_id' => md5($key)
                        ), array('_id' => md5($key)
                    , "stamp" => new MongoDate()
                    , 'url' => $key
                    , 'data' => $data)
                        , array("upsert" => true));
    }

    /**
     * Overrides Get Cache System
     * @param <string> $key     $lookup id
     * @return <array>          
     */
    function getCache($key) {
        $query = array('_id' => md5($key));
        $cursor = self::selectDB("cache")->
                        {arch::cls('cache')->organizer}->
                        findOne($query);
        return $cursor;
    }

    /**
     * Overrides Delete Cache System
     * @param <string> $key     $lookup id
     * @return <boolean>        successful or not
     */
    function delCache($key) {
        $query = array('_id' => new MongoId(md5($key)));
        $cursor = self::selectDB("cache")->
                        {arch::cls('cache')->organizer}->
                        remove($query, array("justOne" => true));
        return $cursor;
    }

}
/**
 * Load Mongo DB Plugin
 * If not, then it won't load.
 */
arch::cls('mdb');

SkipClassLoad:
?>