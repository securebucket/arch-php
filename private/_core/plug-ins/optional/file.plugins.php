<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/docs#4eb6e35011aec61146000006
 */

class file {

    /**
     * These override the default caching system.
     * @param <string> $fileName     $cache uniqueid
     * @param <string> $data    $data to be cached
     * @return <boolean>        successul or not.
     */
    function putCache($fileName, $data) {

        if (!is_writable($arch->cls('cache')->organizer)) {
            trigger_error($arch->cls('cache')->
                    organizer . $fileName . ' is not writable<br>');
            return '';
        }
        $fh = fopen($arch->cls('cache')->organizer . $fileName, "w");
        fwrite($fh, utf8_encode($data));
        fclose($fh);
    }

    /**
     * Overrides Get Cache System
     * @param <string> $file     $lookup id
     * @return <array>
     */
    function getCache($file) {
        $handle = fopen($arch->cls('cache')->organizer . $file, "r");
        $contents = fread($handle, filesize($file));
        fclose($handle);

        return $contents;
    }

    /**
     * Overrides Delete Cache System
     * @param <string> $file     $lookup id
     * @return <boolean>        successful or not
     */
    function delCache($file) {
        return unlink($arch->cls('cache')->organizer . $file);
    }

}
?>