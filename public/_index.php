<?php
/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/
 */

    /**
     * Get Settings from Config file
     */
    if(!file_exists('config.inc.php')) die('setup needed.');
    else require('config.inc.php');

    /**
     * Load Arch
     */
    require DIR_CORE . '_boot.php';

    /**
     * Start Listening to your commands.
     */
    require DIR_CONTROLLER . '_boot.php';

?>