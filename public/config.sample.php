<?php

/*
  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
  @author     Arch PHP - Richard Wagener <code@securebucket.com>

  http://archphp.org/
 */

define('ADMIN_NAME', '');
define('ADMIN_EMAIL', '');
define('SERVER_DOMAIN', '');

if (!ini_get('display_errors')) {
    ini_set('display_errors', 1);
}
/**
 * mySQL Database Info
 */
// Database User Name
define('DB_MYSQL_DBUSER', '');

// Database password
define('DB_MYSQL_DBPASS', '');

// Database Name
define('DB_MYSQL_DBNAME', '');

// database host
define('DB_MYSQL_HOST', 'localhost');

// database type
define('DB_MYSQL_TYPE', 'mysql');

/**
 * MongoDB Database Info
 */
// Database Username
define('DB_MONGODB_DBUSER', '');

// Database Password
define('DB_MONGODB_DBPASS', '');

// Database Host
define('DB_MONGODB_HOST', 'localhost');

/**
 * Core Directory Configurations
 */
// Store files not meant to be accessed publicly.
define('DIR_PRIVATE', '../private/');


// Stores the core files used by Arch PHP
define('DIR_CORE', DIR_PRIVATE . '_core/');


// Stores the core files needed to run arch.
define('DIR_CORE_REQUIRED', DIR_CORE . 'plug-ins/_required/');



// Stores the core plugins that are optional.  like mysql, mongodb, images
define('DIR_CORE_OPTIONAL', DIR_CORE . 'plug-ins/optional/');


/**
 * Your Directory Configurations
 */
// Stores your Controllers.  This directory will never be touched by updates.
define('DIR_CONTROLLER', DIR_PRIVATE . 'mvc/controllers/');


// Stores your Models.  This directory will never be touched by updates.
define('DIR_MODELS', DIR_PRIVATE . 'mvc/models/');


// Stores your Models.  This directory will never be touched by updates.
define('DIR_VIEWS', DIR_PRIVATE . 'mvc/views/');



define('DIR_VIEWS_CORE_CSS', DIR_VIEWS . '_core/css/');

define('DIR_VIEWS_CORE_JS', DIR_VIEWS . '_core/js/');

// Stores your custom plugins.  DIR_VIEWS . "_core/css"
define('DIR_PLUGINS', DIR_PRIVATE . 'plug-ins/');


// Stores the media information.  Publically accessible.
define('DIR_MEDIA', 'media/');

define('DIR_IMAGES', DIR_MEDIA . 'images/');


// font files are used for watermark over images.
define('DIR_FONTS', DIR_MEDIA . 'fonts/');


// vault for original images (before processed)
define('DIR_VAULT_ORIG_IMAGES', DIR_PRIVATE . 'vault/images/');

// vault
define('DIR_VAULT', DIR_PRIVATE . 'vault/');

/*
 * Set Error Pages
 */
define('ERROR_PAGES_404', '/e/404');  // not found
define('ERROR_PAGES_403', '/e/403');  // forbidden
define('ERROR_PAGES_500', '/e/500');  // fatal error

/**
 * Optional Modules to Load
 */
// this will load the integrated mysql class.  Uses PDO.
define('CORE_OPT_PLUGIN_MYSQL', '0');
define('CORE_OPT_PLUGIN_MYSQL_TYPE', 'mysqli'); // choose mysqli or pdo
// Loads the integrated mongodb class.
// Caching System works with this.  (Recommended)
define('CORE_OPT_PLUGIN_MONGODB', '0');

// Loads the integrated files class.
// Caching System works with this.  (USE MONGO GRIDFS INSTEAD)
define('CORE_OPT_PLUGIN_FILES', '0');

// Loads the integrated image manipulation class.
// This is recommeded to process images in a cron.
// Store Images in GridFS mongodb. (optional)
// @todo: this is still in development
define('CORE_OPT_PLUGIN_IMAGE', '0');


/**
 * CSS / Javascript | Combine, compress, and print.
 * array(),array()
 */
// css files in array
define('CORE_HTML_INCLUDE_CSS', ''); // reset.css,stylesheet.css
// js files array
define('CORE_HTML_INCLUDE_JS', ''); // 'jquery-1.3.2.min.js,global.js,jquery.idTabs.min.js,easySlider1.7.js'



/*
 * You don't need to go below this line.
 * This is checking everything is Good-to-Go
 */

if (!is_dir(DIR_PRIVATE)) {
    trigger_error('Config error: DIR_PRIVATE not found');
    die();
}
if (!is_dir(DIR_CORE)) {
    trigger_error('Config error: DIR_CORE not found');
    die();
}
if (!is_dir(DIR_CORE_REQUIRED)) {
    trigger_error('Config error: DIR_CORE_REQUIRED not found');
    die();
}
if (!is_dir(DIR_CORE_OPTIONAL)) {
    trigger_error('Config error: DIR_CORE_OPTIONAL not found');
    die();
}
if (!is_dir(DIR_CONTROLLER)) {
    trigger_error('Config error: DIR_CONTROLLER not found');
    die();
}
if (!is_dir(DIR_MODELS)) {
    trigger_error('Config error: DIR_MODELS not found');
    die();
}
if (!is_dir(DIR_VIEWS)) {
    trigger_error('Config error: DIR_VIEWS not found');
    die();
}
if (!is_dir(DIR_VIEWS_CORE_CSS)) {
    trigger_error('Config error: DIR_VIEWS_CORE_CSS not found');
    die();
}
if (!is_dir(DIR_VIEWS_CORE_JS)) {
    trigger_error('Config error: DIR_VIEWS_CORE_JS not found');
    die();
}
if (!is_dir(DIR_PLUGINS)) {
    trigger_error('Config error: DIR_PLUGINS not found');
    die();
}
if (!is_dir(DIR_MEDIA)) {
    trigger_error('Config error: DIR_MEDIA not found');
    die();
}
if (!is_dir(DIR_IMAGES)) {
    trigger_error('Config error: DIR_IMAGES not found');
    die();
}
if (!is_dir(DIR_FONTS)) {
    trigger_error('Config error: DIR_FONTS not found');
    die();
}
if (!is_dir(DIR_VAULT_ORIG_IMAGES)) {
    trigger_error('Config error: DIR_VAULT_ORIG_IMAGES not found');
    die();
}
?>